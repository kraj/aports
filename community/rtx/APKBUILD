# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=2023.12.27
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx -t "$pkgdir/usr/bin/"
	install -Dm644 README.md docs/*.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm644 "man/man1/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
e6c0291e00be3c1007ac617cccd145c6d740abb2e89caf1aae5960d0db3c21082f30f2b9a9faa2141c2df2a75262773ba00c6da2ef1d7326f80daaa60e35f7ce  rtx-2023.12.27.tar.gz
"
